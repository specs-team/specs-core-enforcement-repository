Run SVA::default recipe on VM before installing any component.

SVA cookbook requires some dependencies, which can be installed using berks. Berks comes with Chef so you don't need to worry about installing it.

Navigave to SVA cookbook
```bash
berks install
berks upload --no-ssl-verify
```

This will download cookbooks from chef marketplace and install them. Cookbooks are usually located at ~/.berkshelf or in windows c:/users/my_username/.berkshelf
