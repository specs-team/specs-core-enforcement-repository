#
# Cookbook Name:: SVA
# Recipe id:: sva-r18
# Recipe:: Check for updates/upgrades
#
# Copyright 2015, XLAB d.o.o
#
# All rights reserved - Do Not Redistribute
#
directory '/tmp/openSUSE 13.2 Update'
projects_path = node['project_path']

bash 'check_for_upgrades' do
	code <<-EOH
		cd #{projects_path}/specs_enforcement_sva/src
		python enforcement.py upgrade_report
	EOH
end