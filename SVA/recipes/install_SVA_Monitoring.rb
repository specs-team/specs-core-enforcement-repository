#
# Cookbook Name:: SVA
# Recipe id:: sva-r5
# Recipe:: Install SVA Monitoring
#
# Copyright 2015, XLAB d.o.o
#
# All rights reserved - Do Not Redistribute
#

## INSTALLING AND CONFIGURING POSTGRESQL

Chef::Config[:zypper_check_gpg] = false

if File.exist?('/tmp/monitoring.pid') then
	puts "Monitoring already running"
	exit
end

package 'postgresql-devel' do
	action :nothing
end.run_action(:install)

package 'screen'
package 'postgresql-devel'
package 'postgresql'
package 'postgresql-contrib'
package 'python-devel'
package 'python-pip'
package 'git'

## CONFIGURING POSTGRESQL

include_recipe "postgresql::server"
include_recipe "database::postgresql"


bash 'restart_postgresql' do
	cwd '/tmp'
	code <<-EOH
		if ! [ $(sudo -u postgres psql -tAc "SELECT 1 FROM pg_roles WHERE rolname='sva'") == 1 ]; then
			service postgresql restart
		fi
	EOH
end

postgresql_connection_info = {
  :host     => '127.0.0.1',
  :port     => node['postgresql']['config']['port'],
  :username => 'postgres',
  :password => node['postgresql']['password']['postgres'],
}

postgresql_database 'sva' do
  connection postgresql_connection_info
  action     :create
end

postgresql_database_user 'sva' do
  connection postgresql_connection_info
  password 'sva'
  action :create
end

postgresql_database_user 'sva' do
  connection postgresql_connection_info
  database_name 'sva'
  privileges [:all]
  action :grant
end

## DONE INSTALLING AND CONFIGURING POSTGRESQL

projects_path = node['project_path']

## INSTALLING MONITORING COMPONENT

directory "#{projects_path}/specs_monitoring_sva" do
	recursive true
	action :delete
end

directory "#{projects_path}" do
	action :create
end

git "#{projects_path}/specs_monitoring_sva" do
	repository 'https://bitbucket.org/specs-team/specs-mechanism-monitoring-sva'
	revision 'master'
	action :sync
end

git "#{projects_path}/specs_sva_core" do
	repository 'https://bitbucket.org/specs-team/specs-mechanism-enforcement-sva_core'
	revision 'master'
	action :sync
end

bash 'Install SVA Core' do
	cwd "#{projects_path}"
	code <<-EOH
		python -m pip install -e "specs_sva_core/"
	EOH
	ignore_failure true
end

bash 'Install Monitoring requirements' do
	cwd "#{projects_path}"
	code <<-EOH
		python -m pip install -r "specs_monitoring_sva/requirements.txt"
	EOH
end

## DONE INSTALLING MONITORING COMPONENT

## RUNNING MONITORING COMPONENT

require 'json'
require 'net/http'

def build_url(ip, port, additional='')
	return 'http://' + ip.to_s + ':' + port.to_s + '/' + additional.to_s
end

plan_id = node['implementation_plan_id']
plan = search(:implementation_plans, "id:#{plan_id}").first

if plan == nil then
	puts "Implementation Plan with id: #{plan_id} is not present in Implementation plans data bag"
	exit
end

slos = plan['slos']
measurements = plan['measurements']
sla_id = plan['sla_id']
component_id = ''
@plan_json = plan.to_json

vms = plan['pools'].first['vms']
django_ip = '0.0.0.0'
event_hub_ip = plan['monitoring_core_ip']
event_hub_port = plan['monitoring_core_port']

vms.each do |vm|
	ip = vm['public_ip']
	components = vm['components']
	components.each do |component|
		if component['recipe'].include? 'install_SVA_Dashboard'
			django_ip = ip
		end
		if component['recipe'].include? 'install_SVA_Monitoring'
			component_id = component['component_id']
		end
	end
end

dashboard_url = build_url(django_ip, node['django']['port'])
event_hub_url = build_url(event_hub_ip, event_hub_port, 'events/specs')

@host = django_ip  # django
@port = node['django']['port']
@post_ws = "/plan/"

def post
	begin
		code = 404
		req = Net::HTTP::Post.new(@post_ws, initheader = {'Content-Type' =>'application/json'})
		req.body = @plan_json
		begin
			http = Net::HTTP.new(@host, @port)
			http.read_timeout = 10;
			response = http.request(req)
			code = response.code.to_i
		rescue Exception => e
			code = 404
			puts "Waiting for dashboard"
		end
		sleep(2)
	end until code == 200
end

post

report_basic_age_frequency = 0
list_availability_frequency = 0
scanner_availability_frequency = 0
list_age_frequency = 0
repository_availability_frequency = 0
up_report_age_frequency = 0
scan_report_availability_frequency = 0
up_report_availability_frequency = 0
multiplier = 3600

availability_delay = 7200
age_delay = 3600

measurements.each do |measurement|
	msr_id = measurement['msr_id']
	frequency = measurement['frequency'].scan(/\d/).join('').to_i * multiplier
	case msr_id
	when 'report_basic_age_sva_msr1'
		report_basic_age_frequency = frequency
	when 'list_age_sva_msr2'
		list_age_frequency = frequency
	when 'up_report_age_sva_msr4'
		up_report_age_frequency = frequency
	when 'repository_availability_sva_msr5'
		repository_availability_frequency = frequency
	when 'list_availability_sva_msr6'
		list_availability_frequency = frequency
	when 'scanner_availability_sva_msr7'
		scanner_availability_frequency = frequency
	when 'scan_report_availability_sva_msr8'
		scan_report_availability_frequency = frequency
	when 'up_report_availability_sva_msr9'
		up_report_availability_frequency = frequency
	end
end


bash 'configure_monitoring_database' do
	cwd "#{projects_path}"
	code <<-EOH
		python specs_monitoring_sva/src/monitoring.py set_dashboard_url #{dashboard_url}
		python specs_monitoring_sva/src/monitoring.py set_event_hub_url #{event_hub_url}
		python specs_monitoring_sva/src/monitoring.py set_sla_id #{sla_id}
		python specs_monitoring_sva/src/monitoring.py set_component_id #{component_id}
	EOH
end

bash 'run_monitoring' do
	cwd "#{projects_path}"
	code <<-EOH
		screen -S monitoring -m -d python specs_monitoring_sva/src/monitoring.py run_monitoring #{report_basic_age_frequency} #{list_availability_frequency} #{scanner_availability_frequency} #{list_age_frequency}  #{repository_availability_frequency} #{up_report_age_frequency} #{scan_report_availability_frequency} #{up_report_availability_frequency} #{availability_delay} #{age_delay} 2>&1
	EOH
end