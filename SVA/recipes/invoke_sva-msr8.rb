#
# Cookbook Name:: SVA
# Recipe id:: sva-r12
# Recipe:: Invoke SVA Monitoring to take measurement sva-mrs8 and label the event as remediation-event.
#
# Copyright 2015, XLAB d.o.o
#
# All rights reserved - Do Not Redistribute
#

bash 'invoke_sva-msr8' do
	code <<-EOH
		cd #{node['project_path']}/specs_monitoring_sva/src
		python monitoring.py invoke_msr8
	EOH
end