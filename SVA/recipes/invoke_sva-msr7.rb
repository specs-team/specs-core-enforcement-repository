#
# Cookbook Name:: SVA
# Recipe id:: sva-r9
# Recipe:: Invoke SVA Monitoring to take measurement sva-mrs7 and label the event as remediation-event.
#
# Copyright 2015, XLAB d.o.o
#
# All rights reserved - Do Not Redistribute
#

bash 'invoke_sva-msr7' do
	code <<-EOH
		cd #{node['project_path']}/specs_monitoring_sva/src
		python monitoring.py invoke_msr7
	EOH
end