#
# Cookbook Name:: SVA
# Recipe id:: sva-r1
# Recipe:: Install OpenScap
#
# Copyright 2015, XLAB d.o.o
#
# All rights reserved - Do Not Redistribute
#

package 'openscap-utils'