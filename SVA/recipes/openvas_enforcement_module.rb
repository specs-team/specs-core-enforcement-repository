if File.exist?('/tmp/openvas_enforcement') then
	puts "Enforcement already installed"
	exit
end

cookbook_file "openVAS_monitoring.py" do
	path "/tmp/openVAS_monitoring.py"
	action :create
end

plan_id = node['implementation_plan_id']
plan = search("implementation_plans","id:#{plan_id}").first

if plan == nil then
puts "Implementation Plan with id: #{plan_id} is not present in Implementation plans data bag"
exit
end

# get EventHub information
EventHubIP=plan['context']['monitoring-core-ip']
EventHubPort=plan['context']['monitoring-core-port']

if EventHubIP == nil or EventHubPort == nil
puts "Insert monitoring-core-ip and monitoring-core-port in context tag of implementation plan"
exit
end


# Download package openvas client

cookbook_file  "openvas-libs-libraries-cli.tar.gz" do
   path "/opt/openvas-libs-libraries-cli.tar.gz"
   action :create
end

# Download package openvas xml utilities  

cookbook_file  "openvas-xml-utilities.tar.gz" do
   path "/opt/openvas-xml-utilities.tar.gz"
   action :create
end

# Download installer script

cookbook_file "initial_libraries-cli_script.sh" do
   path "/opt/initial_libraries-cli_script.sh"
   action :create
end

# Download java_manager script
 
cookbook_file "openvas-monitoring-client.jar" do
   path "/opt/openvas-monitoring-client.jar"
   action :create
end


# unzip tar file & execute installer script

bash "install_program" do
        user "root"
        cwd  "/opt"
        code <<-EOH
        tar -zxvf openvas-libs-libraries-cli.tar.gz
	tar -zxvf openvas-xml-utilities.tar.gz
 	tar -zxvf java_component_openvas_client.tar.gz
	chmod +x initial_libraries-cli_script.sh 
	./initial_libraries-cli_script.sh 
       
	# install java jdk
	zypper -n in '*openjdk*devel*'
	ln -s /opt/openvas-libs/gpgme/lib64/libgpgme.so.11 /usr/lib64/libgpgme.so.11
        ln -s /opt/openvas-libs/libpcap/lib64/libpcap.so.1 /usr/lib64/libpcap.so.1
       
	rm -r openvas-libs-libraries-cli.tar.gz
        rm -r openvas-xml-utilities.tar.gz
        rm -r java_component_openvas_client.tar.gz
        rm -r initial_libraries-cli_script.sh

	EOH
end
 
# get openvas agents ip address
plan['IaaS']['VMs']['nodes'].each do | node |
  node['recipes'].each do | recipe |
   if (recipe['cookbook']=="specs-monitoring-openvas" and recipe['name']=="manager")

        IP_Address=node['private-ips'].first

        # starting vulnerabity scan for each monitored target VM
		bash "execute_scanning" do
			user "root"
			cwd  "/opt"
			code <<-EOH
				java -jar openvas-monitoring-client.jar #{IP_Address} 9390 POST 172.16.117.169 8000 openvas_report_post/ > #{IP_Address}-scan.log &
			EOH
        end
#		cron 'openvas_scanning_#{IP_Address}' do
#			path "/opt"
#			hour "*/24"
#			user "root"
#			command "java -jar openvas-monitoring-client.jar #{IP_Address} 9390 POST 172.16.117.169 8000 openvas_report_post/ > #{IP_Address}-scan.log"
#		end
   end
  end
end

file '/tmp/openvas_enforcement' do
	content 'Enforcement installed'
end