#
# Cookbook Name:: SVA
# Recipe id:: sva-r13
# Recipe:: Delete old scanning report
#
# Copyright 2015, XLAB d.o.o
#
# All rights reserved - Do Not Redistribute
#

file '/tmp/openSUSE 13.2 Update/results.xml' do
	action :delete
end