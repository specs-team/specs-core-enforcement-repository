import time
import threading
import requests
from requests.exceptions import ConnectionError, ConnectTimeout


def run_in_periods(func):
    """
    :param func: function to run in background every n seconds, function is required to have frequency parameter
    :return: function itself, which executes additional stuff written in that function
    """
    def func_wrapper(self, frequency, delay=0, event_type='event'):
        if frequency > 0:
            time.sleep(delay)
            threading.Timer(frequency, func_wrapper, [self, frequency]).start()
        return func(self, frequency, delay, event_type)
    return func_wrapper


def send_post(url, data=None, files=None):
    try:
        response = requests.post(url, data=data, files=files, timeout=10)
        response_code = 200
    except (ConnectionError, ConnectTimeout) as e:
        print "ERROR SENDING TO: %s" % url
        response_code = 404
    return response_code