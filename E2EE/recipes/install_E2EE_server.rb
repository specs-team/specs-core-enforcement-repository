if File.exist?('/tmp/install_E2EE_server') then
	puts "Recipe already ran"
	return
end

plan_id = node['implementation_plan_id']
plan = search(:implementation_plans, "id:#{plan_id}").first
if plan == nil then
	puts "Implementation Plan with id: #{plan_id} is not present in Implementation plans data bag"
	exit
end
vms = plan['pools'].first['vms']
current_ip = ''
begin
	current_ip = node[:network][:interfaces][:eth1][:addresses].detect{|k,v| v[:family] == "inet" }.first
rescue
	current_ip = node[:cloud_v2][:public_ipv4]
end

master_ip = ''
slave_ip = ''
is_backup = false

vms.each do |vm|
	ip = vm['public_ip']
	components = vm['components']
	components.each do |component|
		if component['component_id'] == 'DBB_main_db'
			master_ip = ip
		end
		if component['component_id'] == 'DBB_backup_db'
			slave_ip = ip
		end
		if component['component_id'] == 'DBB_backup_server'
			if current_ip == ip
				is_backup = true
			end
		end
	end
end

node.default['master_ip'] = master_ip
node.default['postgresql']['config']['listen_addresses'] = master_ip
if is_backup
	node.default['postgresql']['config']['listen_addresses'] = slave_ip
end

include_recipe "apt"
include_recipe "golang"
include_recipe "golang::packages"
include_recipe "E2EE::setup_ssl_cert"
include_recipe "E2EE::setup_config"
include_recipe "E2EE::install_E2EE_prerequisites"

file '/tmp/install_E2EE_server'