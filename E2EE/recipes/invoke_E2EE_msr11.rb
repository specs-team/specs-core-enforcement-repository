include_recipe "E2EE::clone_monitoring_component"

def build_url(ip, port, additional='')
	return 'http://' + ip.to_s + ':' + port.to_s + '/' + additional.to_s
end

plan_id = node['implementation_plan_id']
plan = search(:implementation_plans, "id:#{plan_id}").first

if plan == nil then
	puts "Implementation Plan with id: #{plan_id} is not present in Implementation plans data bag"
	exit
end

slos = plan['slos']
sla_id = plan['sla_id']
component_id = ''
vms = plan['pools'].first['vms']
event_hub_ip = plan['monitoring_core_ip']
event_hub_port = plan['monitoring_core_port']
auditor_ip = '0.0.0.0'

event_hub_url = build_url(event_hub_ip, event_hub_port, 'events/specs')

vms.each do |vm|
	ip = vm['public_ip']
	components = vm['components']
	components.each do |component|
		if component['component_id'] == 'DBB_auditor'
			auditor_ip = ip
		end
		if component['component_id'] == 'install_E2EE_Monitoring_Adapter'
			component_id = component['component_id']
		end
	end
end

bash 'check_if_auditor_is_running' do
	code <<-EOH
		python /opt/specs-mechanism-monitoring-e2ee-adapter/monitor.py -i auditor #{auditor_ip} #{node['auditor']['port']} #{event_hub_url}
	EOH
end