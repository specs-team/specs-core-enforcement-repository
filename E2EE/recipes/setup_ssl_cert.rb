#
# Cookbook Name:: e2ee
# Recipe:: setup_keys
#	Sets up SSL certificate for http use with E2EE Server 
#
# Copyright 2016, XLAB
#
# All rights reserved - Do Not Redistribute
#

require 'openssl'
require 'rubygems'

cert_path = "#{node['e2ee']['https']['cert_path']}"
cert_name = "#{node['e2ee']['https']['cert_name']}"
current_ip = ''
begin
	current_ip = node[:network][:interfaces][:eth1][:addresses].detect{|k,v| v[:family] == "inet" }.first
rescue
	current_ip = node[:cloud_v2][:public_ipv4]
end

openssl_x509 "#{cert_path}/#{cert_name}" do
  common_name "#{current_ip}"
  org 'xlab-si'
  org_unit 'SPECS'
  country 'SI'
end