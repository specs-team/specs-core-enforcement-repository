include_recipe "E2EE::clone_monitoring_component"

def build_url(ip, port, additional='')
	return 'http://' + ip.to_s + ':' + port.to_s + '/' + additional.to_s
end

plan_id = node['implementation_plan_id']
plan = search(:implementation_plans, "id:#{plan_id}").first

if plan == nil then
	puts "Implementation Plan with id: #{plan_id} is not present in Implementation plans data bag"
	exit
end

slos = plan['slos']
sla_id = plan['sla_id']
main_server_id = ''
backup_server_id = ''
main_database_id = ''
backup_database_id = ''
auditor_id = ''
monitoring_id = ''
vms = plan['pools'].first['vms']
event_hub_ip = plan['monitoring_core_ip']
event_hub_port = plan['monitoring_core_port']
e2ee_ip = ''
auditor_ip = ''
e2ee_backup_ip = ''
database = ''
database_backup = ''

event_hub_url = build_url(event_hub_ip, event_hub_port, 'events/specs')

vms.each do |vm|
	ip = vm['public_ip']
	components = vm['components']
	components.each do |component|
		if component['recipe'].include? 'install_SVA_Dashboard'
			django_ip = ip
		end
		if component['recipe'].include? 'install_SVA_Monitoring'
			component_id = component['component_id']
		end
	end
end

vms.each do |vm|
	ip = vm['public_ip']
	components = vm['components']
	components.each do |component|
		if component['component_id'] == 'DBB_main_server'
			e2ee_ip = ip
			main_server_id = component['component_id']
		end
		if component['component_id'] == 'DBB_auditor'
			auditor_ip = ip
			auditor_id = component['component_id']
		end
		if component['component_id'] == 'DBB_backup_server'
			e2ee_backup_ip = ip
			backup_server_id = component['component_id']
		end
		if component['component_id'] == 'DBB_main_db'
			database = ip
			main_database_id = component['component_id']
		end
		if component['component_id'] == 'DBB_backup_db'
			database_backup = ip
			backup_database_id = component['component_id']
		end
		if component['component_id'] == 'DBB_monitoring_adapter'
			component_id = component['component_id']
			monitoring_id = component['component_id']
		end
	end
end


cron 'Monitor e2ee' do
	command "python /opt/specs-mechanism-monitoring-e2ee-adapter/src/monitor.py server #{e2ee_ip} #{node['e2ee']['port']} #{monitoring_id} #{sla_id} #{event_hub_url}"
end

cron 'Monitor auditor' do
	command "python /opt/specs-mechanism-monitoring-e2ee-adapter/src/monitor.py auditor #{auditor_ip} #{node['auditor']['port']} #{monitoring_id} #{sla_id} #{event_hub_url}"
end

cron 'Monitor e2ee backup' do
	command "python /opt/specs-mechanism-monitoring-e2ee-adapter/src/monitor.py server_backup #{e2ee_backup_ip} #{node['e2ee_backup']['port']} #{monitoring_id} #{sla_id} #{event_hub_url}"
end

cron 'Monitor database' do
	command "python /opt/specs-mechanism-monitoring-e2ee-adapter/src/monitor.py database #{database} #{node['database']['port']} #{monitoring_id} #{sla_id} #{event_hub_url}"
end

cron 'Monitor backup database' do
	command "python /opt/specs-mechanism-monitoring-e2ee-adapter/src/monitor.py database_backup #{database_backup} #{node['database_backup']['port']} #{monitoring_id} #{sla_id} #{event_hub_url}"
end