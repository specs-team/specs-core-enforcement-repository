When storing data on cloud, the users usually have to accept the risk of security incidents and failures related to modifications and loss of stored data. 

To offer the users the secure storage solution and to allow them not only to detect but also prove violations related to modification and loss of stored data, SPECS is offering two security mechanisms: Database and Backup as a Service (DBB) and End to End Encryption (E2EE).

E2EE:

* client-side encryption enforcing confidentiality and integrity

DBB:

* detection and proof of violations related to write-serializability and read-freshness
* backup of stored data

E2EE cookbook requires some dependencies, which can be installed using berks. Berks comes with Chef so you don't need to worry about installing it.

Navigave to E2EE cookbook
```bash
berks install
berks upload --no-ssl-verify --force
```

This will download cookbooks from chef marketplace and install them. Cookbooks are usually located at ~/.berkshelf or in windows c:/users/my_username/.berkshelf


Procedure for creating master-slave replication:

1. Run on master:

Installs `postgresql` on VM

```bash
sudo chef-client -o "recipe[E2EE::install_postgresql]"
```
2. Then run on slave:
	
Installs `postgresql`, sends `ssh key` to master for passwordless ssh login and configures `postgresql` to act as slave for replication. This recipe does not start postgresql on slave!
```bash
sudo chef-client -o "recipe[E2EE::install_postgresql]"
sudo chef-client -o "recipe[E2EE::postgresql_slave]"
```
3. Then run on master:
	
Sends `ssh key` to slave for passwordless ssh login and configures `postgresql` to act as master for replication, sends inital backup to slave and starts `postgresql` on master
```bash
sudo chef-client -o "recipe[E2EE::postgresql_master]"
```
4. slave:
	
Finally start `postgresql` on slave
```bash
sudo chef-client -o "recipe[E2EE::start_postgresql]"
```
 If master stops working simply run `turn_to_master` recipe on slave VM:
```bash
sudo chef-client -o "recipe[E2EE::turn_to_master]"
```
Create new slave VM and run recipe from step 2. to step 4.