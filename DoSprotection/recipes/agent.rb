#
# Cookbook Name:: specs-monitoring-ossec
# Recipe:: agent
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# get implementation plan
var_plan_id = node['implementation_plan_id']
# old plan
#plan = search("SPECS","id:#{var_plan_id}").first
# new plan
plan = search("implementation_plans","id:#{var_plan_id}").first

if plan == nil then
   puts "Implementation Plan with id: #{var_plan_id} is not present in Implementation plans data bag"
   exit
end

# get ossec server ip address
# old plan
#plan['IaaS']['VMs']['nodes'].each do | node |
#  node['recipes'].each do | recipe |
#   if (recipe['cookbook']=="specs-monitoring-ossec" and recipe['name']=="server")
#   $ossecServer=node['private-ips'].first
#   end
#  end
#end

# new plan
plan['pools'].each do | pool |
  pool['vms'].each do | vm |
    vm['components'].each do | component |
      	   if (component['cookbook']=="DoSprotection" and component['recipe']=="server")
         	$ossecServer = component['private_ips'].first
		puts "ip server: #{$ossecServer}"
	   end
    end
  end
end

if $ossecServer == nil 
  puts "You need acquire a node to install an ossec server ( recipe DoSprotection::server )"
  exit
end

# Download package ossec agent

cookbook_file  "Agent.tar.gz" do
   path "/tmp/ossec-agent.tar.gz"
   action :create
end

# Download installer script

cookbook_file "scripts.tar.gz" do  
   path "/tmp/ossec-scripts.tar.gz"
   action :create
end

bash "get_ssh_credentials" do
  user "root"
  cwd "/root"
  code <<-EOH
    if [ -f /mos/etc/mos/environment.sh ];then
      zypper -n install wget 
      mkdir -p /root/.ssh
      source /mos/etc/mos/environment.sh
      server=$(cat /etc/chef/client.rb | grep chef_server_url)
      IFS=// read -a myarray <<< "$server"
      server_name=$(echo ${myarray[2]})
      complete_name=$(echo http://$server_name:81/mos/var/mos-chef-server-core/ssh-key -O /root/.ssh/id_rsa)
      wget $complete_name
      complete_name2=$(echo http://$server_name:81/mos/var/mos-chef-server-core/ssh-key.pub -O /root/.ssh/id_rsa.pub)
      wget $complete_name2
      chmod 600 /root/.ssh/id_rsa
      chmod 644 /root/.ssh/id_rsa.pub
      cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys
      chmod 600 /root/.ssh/authorized_keys
    fi
  EOH
  not_if { ::File.exists?("/root/.ssh/id_rsa") }
  returns [0, 1]
end

# unzip tar file and execute installer script
bash "install_program" do
        user "root"
        cwd  "/opt"
        code <<-EOH
        	tar -zxvf /tmp/ossec-agent.tar.gz
        	rm /tmp/ossec-agent.tar.gz
        	tar -xvf /tmp/ossec-scripts.tar.gz
        	rm /tmp/ossec-scripts.tar.gz
			touch /var/log/haproxy.log
        	chmod +x ossec-scripts/InstallAgent.sh 
        	chmod +x ossec-scripts/InsertAgentKey.sh
        	chmod +x ossec-scripts/Installsyslog.sh
        	#if syslog is not installed or not running then it is installed and started
        	ossec-scripts/Installsyslog.sh
          	ossec-scripts/InstallAgent.sh #$ossecServer
        	zypper -n in expect
        	rm -r ossec-agent
        EOH
        not_if { ::File.exists?("/opt/ossec") }
end
