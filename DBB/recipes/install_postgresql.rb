#
# Cookbook Name:: E2EE
# Recipe id::  e2ee-r2
# Recipe:: Install E2EE Main DB
#
# Copyright 2015, XLAB d.o.o
#
# All rights reserved - Do Not Redistribute
#

include_recipe "postgresql::server"
include_recipe "database::postgresql"

file '/tmp/install_postgresql'