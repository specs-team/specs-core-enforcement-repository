include_recipe "DBB::clone_monitoring_component"

cron 'monitor_postgres' do
	command "python /opt/specs-mechanism-monitoring-e2ee-adapter/monitor.py database #{node['event_hub_url']} #{node['postgresql']['ip']} #{node['postgresql']['port']}"
end

cron 'monitor_postgres_backup' do
	command "python /opt/specs-mechanism-monitoring-e2ee-adapter/monitor.py database_backup #{node['event_hub_url']} #{node['postgresql_backup']['ip']} #{node['postgresql_backup']['port']}"
end
