Navigave to DBB cookbook
```bash
berks install
berks upload --no-ssl-verify --force
```

This will download cookbooks from chef marketplace and install them. Cookbooks are usually located at ~/.berkshelf or in windows c:/users/my_username/.berkshelf
