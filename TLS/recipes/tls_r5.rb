#
# Cookbook Name:: tls
# Recipe:: tls_r2
#
# Copyright 2010-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Institute e-Austria, Timisoara", http://www.ieat.ro/ .
#
# Developers:
#  * Silviu Panica <silviu@solsys.ro>, <silviu.panica@e-uvt.ro>
#
######
##    enforcement recipe -- Reconfigure TLS crypto strength to M3_value.
######

include_recipe "TLS::tls_r2"

tls_r_configurator_args = "--m3-plus"

specs_slos = node.default['tls_slos']
specs_slos.each do |slo|
  if slo['metric_id'] == "tls_crypto_strenght_m3" then
    if ['geq', 'eq'].include? slo['operator'] then
      if slo['value'].to_i >= 7 then
        tls_r_configurator_args = " --m3-plus"
      else
        tls_r_configurator_args = " --m3"
      end
    elsif ['leq', 'lt'].include? slo['operator'] then
      if slo['value'].to_i <= 6 then
        tls_r_configurator_args = " --m3"
      else
        tls_r_configurator_args = " --m3-plus"
      end
    end
  end
end

#### DO NOT EDIT BELOW THIS LINE
tls_r_configurator_args += node.default['tls_terminator_backend']
bash "generate_tls_terminator_configuration_file" do
    user "root"
    cwd "/tmp"
    code <<-EOH
    source /etc/profile
    /opt/specs-mechanism-enforcement-tls/bin/tls-configurator.sh #{tls_r_configurator_args}
    EOH
    notifies :restart, 'service[specs-mechanism-enforcement-tls-terminator]', :immediately
    returns [0, 1]
end
puts "[TLS Enforcement] Running of tls_rX successful."
####
