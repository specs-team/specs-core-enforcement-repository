##
# Cookbook Name:: tls
# Recipe:: tls_r1
#
# Copyright 2010-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Institute e-Austria, Timisoara", http://www.ieat.ro/ .
#
# Developers:
#  * Silviu Panica <silviu@solsys.ro>, <silviu.panica@e-uvt.ro>
#
######
##    TLS PROBER -- Install
######

include_recipe "TLS::tls_r2"

tls_component_plan = { :sla_id => node.default['tls_sla_id'],
                       :monitoring_core_ip => node.default['tls_monitoring_core_ip'],
                       :monitoring_core_port => node.default['tls_monitoring_core_port'],
                       :prober_id => node.default['tls_component_id'],
                       :slos => node.default['tls_slos'],
                       :measurements => node.default['tls_measurements']
                     }
file '/tmp/tls-adaptor-simplified-plan.json' do
  content JSON.pretty_generate(tls_component_plan)
end

service "specs-mechanism-enforcement-tls-prober" do
  action :nothing
end

bash "tls-prober-start" do
    user "root"
    cwd "/tmp"
    code <<-EOH
    if [ -f /opt/specs-mechanism-enforcement-tls/bootstrap-adaptor.sh ];then
        chmod +x /opt/specs-mechanism-enforcement-tls/bootstrap-adaptor.sh
        /opt/specs-mechanism-enforcement-tls/bootstrap-adaptor.sh start
    fi
    EOH
end
####



puts "[TLS Prober] Installation complete"
