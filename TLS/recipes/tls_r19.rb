#
# Cookbook Name:: tls
# Recipe:: tls_r4
#
# Copyright 2010-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Institute e-Austria, Timisoara", http://www.ieat.ro/ .
#
# Developers:
#  * Silviu Panica <silviu@solsys.ro>, <silviu.panica@e-uvt.ro>
#
######
##    TLS Endpoint restart - WebPool Haproxy
######

#### this snippet was taken from WebPool::haproxy.rb
bash "execute_program_killprocess_haproxy" do
    user "root"
    code <<-EOH
    ps axf | grep /opt/haproxy-1.5.9/haproxy | grep -v grep | awk '{print "kill -9 " $1}' | sh
    sleep 2
    rm -f /opt/haproxy/haproxysock
    /opt/haproxy-1.5.9/haproxy -f /opt/haproxy/configs/haproxy.cfg
    EOH
end
####

puts "TLS Endpoint restarted."
