# SPECS Chef Repository - Enforcement

The Enforcement repository consists of a set of cookbooks to be used to deploy, configure and control the Enforcement components using [Chef technology](http://chef.io/).

The Enforcement components offers support for the SPECS Security Mechanisms.

# Repository structure

The Chef Repository contains a set of directories where all the cookbooks are described. 
Each cookbook must have its own directory and the name of the directory represents the name of the cookbook. The cookbook must be define in accordance with [Chef cookbooks guidelines](https://docs.chef.io/cookbook_repo.html)

## Default cookbook structure

* specs-core-enforcement-repository/
  * cookbook_name/
    * attributes/
    * files/
    * recipes/
    * metadata.rb
    * README.md

ATTENTION: **metadata.rb** must contain the cookbook description and it is mandatory to be created (otherwise the cookbook will be rejected by the Chef Server/Client tools).

# Repository cookbooks

The current Enabling Platform cookbooks are:

* **DBB/** -- SPECS Enforcement - DBB Security Mechanism
* **DoSprotection/** -- SPECS Enforcement - DoSprotection Security Mechanism
* **E2EE/** -- SPECS Enforcement - E2EE Security Mechanism
* **SVA/** -- SPECS Enforcement - SVA Security Mechanism
* **TLS/** -- SPECS Enforcement - TLS Security Mechanism
