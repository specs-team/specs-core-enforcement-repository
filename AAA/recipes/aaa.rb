#
# Cookbook Name:: applications
# Recipe:: web-container-app
#
# Copyright 2010-2015, IeAT, Romania, http://ieat.ro/
# Copyright 2010-2015, CeRICT, Italia, http://cerict.it/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "CeRICT", http://url/ .
#
# Developers:
#  * Massimiliano Rak <massimiliano.rak@unina2.it>
#  * Silviu Panica <silviu.panica@e-uvt.ro>
#

include_recipe "enabling-platform::apache-tomcat-v7"
include_recipe "AAA::user-manager-conf"

remote_file '/opt/mos-apache-tomcat-v7/webapps/user-manager.war' do
  source 'http://ftp.specs-project.eu/public/artifacts/vertical-layer/user-manager/oauth-server-0.0.1-SNAPSHOT.war'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
  only_if { ::File.exists?("/opt/mos-apache-tomcat-v7/cmd/bootstrap")  and ! ::File.exists?("/opt/mos-apache-tomcat-v7/webapps/user-manager") }
end

#Include template
template "/opt/mos-apache-tomcat-v7/webapps/user_manager.properties" do
    action :create
    source "user_manager_properties.erb"
              not_if { ::File.exists?("/opt/mos-apache-tomcat-v7/webapps/user_manager.properties") }
end

#Set adapter
# get implementation plan
var_plan_id = node['implementation_plan_id']
# old plan
# plan = search("implementation_plans","id:#{var_plan_id}").first
# new plan
plan = search("implementation_plans","id:#{var_plan_id}").first

if plan == nil then
    puts "Implementation Plan with id: #{var_plan_id} is not present in Implementation plans data bag"
    exit
end

# Download java jdk 7

remote_file  "/tmp/java.tar.gz" do
    source "http://www.java.net/download/jdk7u80/archive/b05/binaries/jdk-7u80-ea-bin-b05-linux-x64-20_jan_2015.tar.gz"
    not_if { ::File.exists?("/opt/jdk1.7.0_80/") }
end

# install jdk

bash "install_jdk" do
    user "root"
    cwd  "/opt"
    code <<-EOH
    
    tar xvzf /tmp/java.tar.gz
    rm /tmp/java.tar.gz
    echo 'export JAVA_HOME=/opt/jdk1.7.0_80/' >> /root/.bashrc
    #set PATH
    echo 'export PATH=$PATH:$JAVA_HOME/bin/' >> /root/.bashrc
    
    EOH
    not_if { ::File.exists?("/opt/jdk1.7.0_80/") }
end

# get EventHub information
EventHubIP=plan['monitoring_core_ip']
EventHubPort=plan['monitoring_core_port']
RefreshPeriod="60000"

PlanID=plan['plan_id']
puts "id plan:"
puts "#{PlanID}"

SlaID=plan['sla_id']
puts "id sla:"
puts "#{SlaID}"

PoolsString= plan['pools'].first.to_json.to_s
PoolsStringEscape= PoolsString.gsub("\"", "\\\"")

SlosString=plan['slos'].to_json
SlosStringEscape= SlosString.gsub("\"", "\\\"")

MeasurementsString=plan['measurements'].to_json
MeasurementsStringEscape= MeasurementsString.gsub("\"", "\\\"")

puts "pools object plan:"
puts "#{PoolsString}"
puts "pools object escape plan:"
puts "#{PoolsStringEscape}"

puts "slos object plan:"
puts "#{SlosString}"
puts "slos object escape plan:"
puts "#{SlosStringEscape}"

puts "measurements object plan:"
puts "#{MeasurementsString}"
puts "measurements object escape plan:"
puts "#{MeasurementsStringEscape}"

PlanConcat = "\"#{PlanID}"+"split_string"+"#{SlaID}"+"split_string"+"#{PoolsStringEscape.to_s}"+"split_string"+"#{SlosStringEscape.to_s}"+"split_string"+"#{MeasurementsStringEscape.to_s}\""
puts "planConcat object plan:"
puts "#{PlanConcat}"


if EventHubIP == nil or EventHubPort == nil
    puts "Insert monitoring-core-ip and monitoring-core-port in context tag of implementation plan"
    exit
end

cookbook_file "aaa-adapter.jar" do
    path "/opt/aaa-adapter.jar"
    action :create
end

bash "execute_aaa_adapter" do
    
    user "root"
    cwd  "/opt"
    environment 'PATH' => "#{ENV['PATH']}:/opt/jdk1.7.0_80/bin"
    code <<-EOH
    
    java -jar aaa-adapter.jar #{PlanConcat} #{EventHubIP} #{EventHubPort} #{RefreshPeriod} > aaa-adapter.log &
    
    
    
    EOH
    not_if { ::File.exists?("/opt/aaa-adapter.log") }
end