#
# Cookbook Name:: AAA
# Recipe:: aaa
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

#Apache tomcat v7 installation
package "mos-apache-tomcat-v7"

cookbook_file "apache-tomcat-v7-tomcat-users.xml" do
    path "/opt/mos-apache-tomcat-v7/conf/tomcat-users.xml"
    action :create
end

cookbook_file "apache-tomcat-v7-server.xml" do
    path "/opt/mos-apache-tomcat-v7/conf/server.xml"
    action :create
end

execute 'start-mos-apache-tomcat-v7' do
    not_if do
        ::File.exists?("/var/run/mos-apache-tomcat-v7.pid")
    end
    command '/opt/mos-apache-tomcat-v7/cmd/bootstrap'
    action :run
end

#AAA deploying
cookbook_file  "specs-oauth2-server.war" do
    path "/opt/mos-apache-tomcat-v7/webapps/specs-oauth2-server.war"
    action :create
    only_if { ::File.exists?("/opt/mos-apache-tomcat-v7/cmd/bootstrap")  and ! ::File.exists?("/opt/mos-apache-tomcat-v7/webapps/specs-oauth2-server.war") }
end