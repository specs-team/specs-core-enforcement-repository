#
# Cookbook Name:: WebPool
# Recipe:: nginx
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe"mysql::default"
# get implementation plan
var_plan_id = node['implementation_plan_id']
# old plan
# plan = search("implementation_plans","id:#{var_plan_id}").first
# new plan
plan = search("implementation_plans","id:#{var_plan_id}").first

if plan == nil then
puts "Implementation Plan with id: #{var_plan_id} is not present in Implementation plans data bag"
exit
end

# install nginx

package 'nginx' do
action :install
end

# install php5

package 'php5-fpm' do
action :install
end

bash "permessi" do
        user "root"
        code <<-EOH
	chmod 1733 /var/lib/php5
 	EOH
end

cookbook_file  "php-fpm.conf" do
   path "/etc/php5/fpm/php-fpm.conf"
   action :create
end

cookbook_file  "nginx.conf" do
   path "/etc/nginx/nginx.conf"
   action :create
end

service 'nginx.service' do
action [:enable,:start]
end

# Edit php  configuration file

template "/etc/php5/fpm/php.ini" do
        action :create
        source "php.ini.erb"
	variables(
        :plan => plan
        )
end

package 'php5-mysql' do
action :install
end

package 'php5-zip' do
action :install
end

package 'php5-curl' do
action :install
end

service 'php-fpm.service' do
action [:enable,:start]
end

# install memcached

package 'memcached' do
action :install
end

# install dependence php-memcached

package 'gcc' do
action :install
end

package 'make' do
action :install
end

package 'php5-devel' do
action :install
end

# Download  php-memcached package
 
cookbook_file  "memcache.tgz" do
   path "/tmp/memcache.tgz"
   action :create
end

# unzip tar file

bash "install_memcache" do
        user "root"
        cwd  "/etc"
        code <<-EOH
        tar -xvzf /tmp/memcache.tgz
        rm /tmp/memcache.tgz
 	EOH
        not_if { ::File.exists?("/etc/package.xml") }
end

# install php-memcached

bash "install_memcache" do
        user "root"
        cwd "/etc/memcache-2.2.7"
        code <<-EOH
        	phpize
		./configure
		make install
	EOH
	end 

# Edit hosts file

template "/etc/hosts" do
        action :create
        source "hosts.erb"
	variables(
        :plan => plan
        )
end

# Install Web Application

cookbook_file  "index.php.nginx" do
   path "/srv/www/htdocs/index.php"
   action :create
end

# install app for manage web app deployed

directory '/srv/www/htdocs/manage_app' do
  owner 'root'
  group 'root'
  mode '0777'
  action :create
end

cookbook_file "index.php.webapp" do
   path "/srv/www/htdocs/manage_app/index.php"
   owner 'root'
   group 'root'
   mode '0777'
   action :create
end

directory '/srv/www/htdocs/manage_app/uploads' do
  owner 'root'
  group 'root'
  mode '0777'
  action :create
end

cookbook_file "unzip.php" do
   path "/srv/www/htdocs/manage_app/uploads/unzip.php"
   owner 'root'
   group 'root'
   mode '0777'
   action :create
end

# Execute programms

bash "execute_programm" do
user "root"
code <<-EOF

    #path to pgrep command
    PGREP="/usr/bin/pgrep"
    HTTPD="nginx"
    # find nginx pid
    $PGREP ${HTTPD}
    if [ $? -ne 0 ] # if nginx  not running
    then

    #To start nginx:

       systemctl start nginx.service

    #To start PHP-FPM:

       systemctl start php-fpm.service

   else

   #To restart nginx
         systemctl reload nginx.service
  
   #To restart PHP-FPM:

        systemctl reload php-fpm.service
 
   fi   

   #To start Memcached:

       /usr/sbin/memcached -u root -l 0.0.0.0 -p 11211 -M -m 64 -d

  
EOF
end

